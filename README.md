## Info

Based on: https://redteaming.co.uk/2020/12/01/worried-about-edr-hooking-catching-you-out/

## Compile

```
cl /c /GS- getapibytes.c /Fogetapibytes.o
```

## Usage

```
beacon> get-api-bytes kernel32.dll CreateRemoteThread
[*] Running GetApiBytes BOF
[+] host called home, sent: 1018 bytes
[+] received output:
Module Name: kernel32.dll

[+] received output:
Function Name: CreateRemoteThread

[+] received output:
4c
[+] received output:
8b
[+] received output:
dc
[+] received output:
48
[+] received output:
83
[+] received output:
ec
[+] received output:
48
[+] received output:
44
```