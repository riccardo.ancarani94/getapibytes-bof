#include <windows.h>
#include <stdio.h>
#include "beacon.h"

DECLSPEC_IMPORT FARPROC WINAPI KERNEL32$GetProcAddress(HMODULE, LPCSTR);
DECLSPEC_IMPORT FARPROC WINAPI KERNEL32$LoadLibraryA(LPCSTR);
DECLSPEC_IMPORT BOOL WINAPI KERNEL32$ReadProcessMemory(HANDLE, LPCVOID, LPVOID, SIZE_T, SIZE_T*);
WINBASEAPI void *__cdecl MSVCRT$memcpy(void * __restrict _Dst,const void * __restrict _Src,size_t _MaxCount);

void go(char *args, int len) {
	datap parser;
	char * moduleName;
	char * functionName;
	unsigned char buf[8];
	SIZE_T bRead = 0;
	BOOL result = FALSE;
	
	FARPROC module = NULL;
	FARPROC funcAddress = NULL;
	
	
	BeaconDataParse(&parser, args, len);
	moduleName = BeaconDataExtract(&parser, NULL);
	functionName = BeaconDataExtract(&parser, NULL);
	BeaconPrintf(CALLBACK_OUTPUT, "Module Name: %s\n", moduleName);
	BeaconPrintf(CALLBACK_OUTPUT, "Function Name: %s\n", functionName);
	
	module = KERNEL32$LoadLibraryA((LPCSTR)moduleName);
	
	if (module != NULL)
	{
		funcAddress = KERNEL32$GetProcAddress((HMODULE)module, (LPCSTR)functionName);
		if (funcAddress != NULL)
		{
			result = KERNEL32$ReadProcessMemory(KERNEL32$GetCurrentProcess(), funcAddress, buf, sizeof(buf), &bRead);
			if (result)
			{
				int i = 0;
				for (i = 0; i  < sizeof(buf); i++)
				{
					BeaconPrintf(CALLBACK_OUTPUT, "%x", buf[i]);
				}

				
			} else {
				BeaconPrintf(CALLBACK_OUTPUT, "ReadProcessMemory failed\n");
			}
		} else {
		BeaconPrintf(CALLBACK_OUTPUT, "Failed to find function address\n");
	}
		
	} else {
		BeaconPrintf(CALLBACK_OUTPUT, "Could not load library\n");
	}
	

}